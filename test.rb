def fuel_for_landing_or_lanch(mass, planet, fuel = 0, land = true)
  next_fuel = if land
                (mass * planet * 0.033).floor - 42
              else
                (mass * planet * 0.042).floor - 33
              end
  if next_fuel.positive?
    fuel_for_landing_or_lanch(next_fuel, planet, fuel + next_fuel, land)
  else
    fuel
  end
end

def weight_of_fuel(mass, *landing_or_lanchs)
  fuel = 0
  landing_or_lanchs[0].reverse.each do |l|
    fuel += fuel_for_landing_or_lanch(mass + fuel, l[1], 0, l[0] == :land)
  end
  fuel
end

p weight_of_fuel(28801, [[:launch, 9.807], [:land, 1.62], [:launch, 1.62], [:land, 9.807]])

p weight_of_fuel(14606, [[:launch, 9.807], [:land, 3.711], [:launch, 3.711], [:land, 9.807]])

p weight_of_fuel(75432, [[:launch, 9.807], [:land, 1.62], [:launch, 1.62], [:land, 3.711],[:launch, 3.711], [:land, 9.807]])
